package com.prototypeback.app.services.impl;

import com.prototypeback.app.database.repositories.UserRepository;
import com.prototypeback.app.models.UserModel;
import com.prototypeback.app.services.interfaces.UserModelService;
import com.prototypeback.app.utilities.exceptions.UserNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserModelServiceImpl implements UserModelService {

    private final UserRepository userRepository;

    public UserModelServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    // Using get to get UserModel instead of Optional<UserModel>
    // using get like this is not a good idea but time
    public UserModel findUserById(String id) {
        return userRepository.findById(id).orElseThrow(
                () -> new UserNotFoundException(id)
        );
    }

    @Override
    public List<UserModel> findAllUsers() {
        return userRepository.count() > 0 ? userRepository.findAll() : null;
    }

    @Override
    public UserModel saveUser(UserModel userModel) {
        return userRepository.save(userModel);
    }

    @Override
    public void deleteUser(String id) {
        userRepository.deleteById(id);
    }

    @Override
    public UserModel updateUser(UserModel userModel) {
        userRepository.deleteById(userModel.getId());
        return userRepository.save(userModel);
    }

}
