package com.prototypeback.app.models.viewModels;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = LoginViewModel.class)
public class LoginViewModel {
    private String userName;
    private String password;

    public LoginViewModel(){

    }

    public LoginViewModel(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    // @TODO review later if we want to allow this
    public void setPassword(String password) {
        this.password = password;
    }
}
