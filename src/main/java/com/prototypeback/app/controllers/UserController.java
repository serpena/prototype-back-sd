package com.prototypeback.app.controllers;

import com.prototypeback.app.models.UserModel;
import com.prototypeback.app.services.interfaces.UserModelService;
import org.hibernate.annotations.common.reflection.XMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RequestMapping(value="/user")
@CrossOrigin
@RestController
public class UserController {

    // Here it should go service instead of repository
    // but due to time we access directly to repository
    private final UserModelService userModelService;

    public UserController(UserModelService userModelService) {
        this.userModelService = userModelService;
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<UserModel> get(@PathVariable String id){
        // Finding  user, if not found throw exception
        return new ResponseEntity<>(this.userModelService.findUserById(id), HttpStatus.OK);
    }

    @GetMapping(value="/getList")
    public ResponseEntity<List<UserModel>> getList(){
        // Getting all users
        return ResponseEntity.ok(this.userModelService.findAllUsers());
    }

    @PostMapping()
    public ResponseEntity<UserModel> create(@RequestBody UserModel newUserModel){
        // Saving user
        UserModel created = new UserModel();

        created.setUserName(newUserModel.getUserName());
        created.setName(newUserModel.getName());
        created.setLastName(newUserModel.getLastName());
        created.setEmail(newUserModel.getEmail());
        created.setPassword(newUserModel.getPassword());
        created.setRoles(newUserModel.getRoles());
        created.setPermissions(newUserModel.getPermissions());

        return new ResponseEntity<>(this.userModelService.saveUser(created), HttpStatus.OK);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<UserModel> edit(@RequestBody UserModel newUserModel, @PathVariable String id){
        // Finding  user, if not found throw exception
        UserModel edited = this.userModelService.findUserById(id);
        edited.setId(id);
        edited.setUserName(newUserModel.getUserName());
        edited.setName(newUserModel.getName());
        edited.setLastName(newUserModel.getLastName());
        edited.setEmail(newUserModel.getEmail());
        edited.setPassword(newUserModel.getPassword());
        edited.setRoles(newUserModel.getRoles());
        edited.setPermissions(newUserModel.getPermissions());

        return new ResponseEntity<>(this.userModelService.updateUser(edited), HttpStatus.OK);
    }

    @DeleteMapping(value= "/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id){
        // Deleting user by ID
        this.userModelService.deleteUser(id);
        return ResponseEntity.ok(null);
    }
}
