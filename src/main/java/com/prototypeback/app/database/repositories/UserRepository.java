package com.prototypeback.app.database.repositories;

import com.prototypeback.app.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserModel, String> {
    UserModel findByUserName(String id);
}
