package com.prototypeback.app.utilities.exceptions;

/**
 * Class to handle user not found, generates exception
 */
public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String id) {
        super("User with Id " + id +" Not Found " );
    }
}
