package com.prototypeback.app.security;

public class JwtProperties {
    public static final String SECRET= "secret";
    public static final String TOKEN_PREFIX= "Bearer ";
    public static final String HEADER_STRING= "Authorization";
    public static final int EXPIRATION_DATE= 172800000; //Time is in milliseconds (2 days)

}
