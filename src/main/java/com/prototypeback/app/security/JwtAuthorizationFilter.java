package com.prototypeback.app.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.prototypeback.app.database.repositories.UserRepository;
import com.prototypeback.app.models.UserModel;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private UserRepository userRepository;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager,
                                  UserRepository userRepository) {
        super(authenticationManager);
        this.userRepository = userRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {

        // Read the Authorization header, where JWT token should be
        String header = request.getHeader(JwtProperties.HEADER_STRING);

        // If header does not contain BEARER or is null delegate to Spring impl and exit
        if(header == null || !header.startsWith(JwtProperties.HEADER_STRING)) {
            chain.doFilter(request, response);
            return;
        }

        // If header is present, get user from DB and perform authorization
        Authentication authentication = getUserNamePasswordAuthentication(request);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Continue filter execution
        chain.doFilter(request, response);
    }

    private Authentication getUserNamePasswordAuthentication(HttpServletRequest request) {
        // Obtaining token
        String token = request.getHeader(JwtProperties.HEADER_STRING);
        UsernamePasswordAuthenticationToken auth = null;
        if(token != null) {
            // parse token and validate
            String userName = JWT.require(Algorithm.HMAC512(JwtProperties.SECRET.getBytes()))
                    .build()
                    .verify(token.replace(JwtProperties.TOKEN_PREFIX, ""))
                    .getSubject();

            // Search in DB by userName
            // If we obtain it, then we obtain user data and we create authentication with user data
            if(userName != null) {
                UserModel userModel = this.userRepository.findByUserName(userName);
                UserAuth userAuth = new UserAuth(userModel);
                auth = new UsernamePasswordAuthenticationToken(userName, null, userAuth.getAuthorities());
            }
        }

        return auth;
    }
}
