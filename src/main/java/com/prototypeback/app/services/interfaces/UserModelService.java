package com.prototypeback.app.services.interfaces;

import com.prototypeback.app.models.UserModel;

import java.util.List;

public interface UserModelService {
    UserModel findUserById(String id);
    List<UserModel> findAllUsers();
    UserModel saveUser(UserModel userModel);
    void deleteUser(String id);
    UserModel updateUser(UserModel userModel);
}
