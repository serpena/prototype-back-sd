package com.prototypeback.app.bootstrap;

import com.prototypeback.app.database.repositories.UserRepository;
import com.prototypeback.app.models.UserModel;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class BootStrapData implements CommandLineRunner {
    private final UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public BootStrapData(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Loading user data");

        UserModel user_sergio = new UserModel();
        user_sergio.setUserName("sergio@Admin");
        user_sergio.setName("Sergio");
        user_sergio.setName("Peña");
        user_sergio.setEmail("sepenab@unirioja.es");
        user_sergio.setPassword(passwordEncoder.encode("passwordSergio"));
        user_sergio.setRoles("ADMIN");
        user_sergio.setPermissions("ACCEPT_ALL");

        userRepository.save(user_sergio);

        UserModel user_1 = new UserModel();
        user_1.setUserName("usuario1");
        user_1.setName("usuario");
        user_1.setName("1");
        user_1.setEmail("usuario@unirioja.es");
        user_1.setPassword(passwordEncoder.encode("passwordUsuario1"));
        user_1.setRoles("ADMIN");
        user_1.setPermissions("ACCEPT_ALL");

        userRepository.save(user_1);

        UserModel user_2= new UserModel();
        user_2.setUserName("usuario2");
        user_2.setName("usuario");
        user_2.setName("2");
        user_2.setEmail("usuario2@unirioja.es");
        user_2.setPassword(passwordEncoder.encode("passwordUsuario2"));
        user_2.setRoles("ADMIN");
        user_2.setPermissions("ACCEPT_ALL");

        userRepository.save(user_2);

        System.out.println("Users saved: " + userRepository.count());
    }
}
