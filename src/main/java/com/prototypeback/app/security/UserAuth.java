package com.prototypeback.app.security;

import com.prototypeback.app.models.UserModel;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserAuth implements UserDetails {
    //Decorator pattern
    private UserModel userModel;

    public UserAuth(UserModel userModel) {
        this.userModel = userModel;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorityList = new ArrayList<>();

        // Getting permissions
        this.userModel.getPermissionList().forEach(
                (permission) -> {
                    GrantedAuthority authority = new SimpleGrantedAuthority(permission);
                    authorityList.add(authority);
                }
        );

        // Getting permissions
        this.userModel.getRoleList().forEach(
                (role) -> {
                    GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + role);
                    authorityList.add(authority);
                }
        );

        return authorityList;
    }

    @Override
    public String getPassword() {
        return this.userModel.getPassword();
    }

    @Override
    public String getUsername() {
        return this.userModel.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
