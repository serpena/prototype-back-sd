package com.prototypeback.app.services;


import com.prototypeback.app.database.repositories.UserRepository;
import com.prototypeback.app.models.UserModel;
import com.prototypeback.app.security.UserAuth;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserModelDetailsService implements UserDetailsService {
    private UserRepository userRepository;

    public UserModelDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserModel userModel = this.userRepository.findByUserName(s);

        return new UserAuth(userModel);
    }
}
